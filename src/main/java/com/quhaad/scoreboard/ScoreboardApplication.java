package com.quhaad.scoreboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ScoreboardApplication {
  public static void main(String[] args) {
    SpringApplication.run(ScoreboardApplication.class, args);
  }
}
