package com.quhaad.scoreboard.controller;

import com.quhaad.scoreboard.service.ScoreboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ScoreboardController {
  @Autowired
  private ScoreboardService scoreboardService;

  @GetMapping(path = "/round-over")
  public String roundOver() {
    scoreboardService.roundOver();
    return "redirect:/scoreboard-api";
  }
}
