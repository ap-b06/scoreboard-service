package com.quhaad.scoreboard.controller;

import com.quhaad.scoreboard.service.ScoreboardServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@WebMvcTest(controllers = ScoreboardRestController.class)
public class ScoreboardRestControllerTest {
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private ScoreboardServiceImpl scoreboardService;

  @Test
  public void whenScoreboardApiIsAccessedItShouldContainJson() throws Exception {
    mockMvc.perform(get("/scoreboard-api"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
  }
}
