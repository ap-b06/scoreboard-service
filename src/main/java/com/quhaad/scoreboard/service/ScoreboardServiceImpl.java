package com.quhaad.scoreboard.service;

import com.quhaad.scoreboard.core.GameRoom;
import com.quhaad.scoreboard.core.Player;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ScoreboardServiceImpl implements ScoreboardService {
  private final GameRoom gameRoom;
  private final Player player1;
  private final Player player2;
  private final Player player3;
  private final Player player4;

  public ScoreboardServiceImpl() {
    this.gameRoom = new GameRoom();
    this.player1 = new Player("Player 1",1,gameRoom);
    this.player2 = new Player("Player 2",2,gameRoom);
    this.player3 = new Player("Player 3",3,gameRoom);
    this.player4 = new Player("Player 4",4,gameRoom);
  }

  @Override
  public void roundOver() {
    getGameRoom().roundOver();
  }

  @Override
  public List<Player> getPlayers() {
    return getGameRoom().getPlayers();
  }

  public GameRoom getGameRoom() {
    return gameRoom;
  }
}
