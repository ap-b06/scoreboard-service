package com.quhaad.scoreboard.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerTest {
  private GameRoom gameroom;
  private Player player1;
  private Player player2;

  @BeforeEach
  public void setUp() {
    gameroom = new GameRoom();
    player1 = new Player("Player1", 1, gameroom);
    player2 = new Player("Player2", 2, gameroom);
    player1.addScore(10);
    player2.addScore(20);
  }

  @Test
  public void testMethodGetName() {
    assertEquals("Player1", player1.getName());
  }

  @Test
  public void testMethodGetRank() {
    assertEquals(1, player1.getRank());
  }

  @Test
  public void testMethodGetScore() {
    assertEquals(10, player1.getScore());
  }

  @Test
  public void testMethodAddScore() {
    player1.addScore(10);
    assertEquals(20, player1.getScore());
  }

  @Test
  public void testMethodCompareTo() {
    assertEquals(10, player1.compareTo(player2));
  }

  @Test
  public void testMethodUpdate() {
    player2.addScore(100);
    gameroom.roundOver();
    assertEquals(1, player2.getRank());
    assertEquals(2, player1.getRank());
  }
}
