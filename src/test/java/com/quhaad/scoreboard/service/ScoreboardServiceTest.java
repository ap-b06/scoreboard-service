package com.quhaad.scoreboard.service;

import com.quhaad.scoreboard.core.GameRoom;
import com.quhaad.scoreboard.core.Player;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.spy;

@ExtendWith(MockitoExtension.class)
public class ScoreboardServiceTest {

  @Mock
  private GameRoom gameRoom;

  @InjectMocks
  private ScoreboardServiceImpl scoreboardService;

  @Test
  public void testWhenRoundOverIsCalledItShouldCallGameRoomRoundOver() {
    ScoreboardServiceImpl scoreboardSpy = spy(scoreboardService);

    when(scoreboardSpy.getGameRoom()).thenReturn(gameRoom);
    scoreboardSpy.roundOver();
    verify(gameRoom, times(1)).roundOver();
  }

  @Test
  public void testWhenGetPlayersIsCalledItShouldReturnListOfPlayers() {
    List<Player> playerList = new ArrayList<>();
    playerList.add(new Player("nama", 1, gameRoom));

    ScoreboardService scoreboardSpy = spy(scoreboardService);
    when(scoreboardSpy.getPlayers()).thenReturn(playerList);

    Iterable<Player> calledPlayer = scoreboardSpy.getPlayers();

    assertThat(calledPlayer).isEqualTo(playerList);
  }
}