package com.quhaad.scoreboard.controller;

import com.quhaad.scoreboard.service.ScoreboardServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

@WebMvcTest(controllers = ScoreboardController.class)
public class ScoreboardControllerTest {
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private ScoreboardServiceImpl scoreboardService;

  @Test
  public void whenRoundOverUrlIsAccessedItShouldCallScoreboardServiceRoundOver() throws Exception {
    mockMvc.perform(get("/round-over"))
        .andExpect(handler().methodName("roundOver"))
        .andExpect(status().is3xxRedirection())
        .andExpect(redirectedUrl("/scoreboard-api"));
  }
}
