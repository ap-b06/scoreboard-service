package com.quhaad.scoreboard.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GameRoom {
  private List<Player> players = new ArrayList<>();
  private Random rand = new Random();

  public void add(Player player) {
    players.add(player);
  }

  public List<Player> getPlayers() {
    return players;
  }

  public void roundOver() {
    addScore();
    Collections.sort(players);
    broadcast();
  }

  public void broadcast() {
    for (Player player: getPlayers()) {
      player.update();
    }
  }

  public void addScore() {
    for (Player player: getPlayers()) {
      player.addScore(rand.nextInt(101));
    }
  }
}
