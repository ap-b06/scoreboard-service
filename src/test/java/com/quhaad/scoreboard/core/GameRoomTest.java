package com.quhaad.scoreboard.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameRoomTest {
  private List<Player> players;
  private Player player1;
  @InjectMocks
  private GameRoom gameRoom;

  @BeforeEach
  public void setUp() {
    players = new ArrayList<>();
    gameRoom = new GameRoom();
    player1 = new Player("Player1", 1, gameRoom);
    players.add(player1);
  }

  @Test
  public void testMethodAddPlayer() {
    gameRoom.add(player1);
    assertEquals(2, gameRoom.getPlayers().size());
  }

  @Test
  public void testGetPlayers() {
    assertEquals(players, gameRoom.getPlayers());
  }

  @Test
  public void testWhenRoundOverIsCalledItShouldCallAddScoreAndBroadcastMethod() {
    GameRoom spiedGameRoom = spy(gameRoom);

    spiedGameRoom.roundOver();
    verify(spiedGameRoom, times(1)).addScore();
    verify(spiedGameRoom, times(1)).broadcast();
  }

  @Test
  public void testWhenBroadcastIsCalledItShouldCallPlayerUpdate() {
    List<Player> players = new ArrayList<>();
    Player player1 = new Player("a", 1, gameRoom);

    Player playerSpy = spy(player1);
    players.add(playerSpy);
    GameRoom spiedGameRoom = spy(gameRoom);

    when(spiedGameRoom.getPlayers()).thenReturn(players);
    spiedGameRoom.broadcast();
    verify(playerSpy, times(1)).update();
  }
}
