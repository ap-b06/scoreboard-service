package com.quhaad.scoreboard.controller;

import com.quhaad.scoreboard.core.Player;
import com.quhaad.scoreboard.service.ScoreboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/scoreboard-api")
public class ScoreboardRestController {

  @Autowired
  private final ScoreboardService scoreboardService;

  public ScoreboardRestController(ScoreboardService scoreboardService) {
    this.scoreboardService = scoreboardService;
  }

  @GetMapping
  public List<Player> getPlayers() {
    return scoreboardService.getPlayers();
  }
}
