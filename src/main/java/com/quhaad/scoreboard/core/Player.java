package com.quhaad.scoreboard.core;

public class Player implements Comparable<Player> {
  protected GameRoom gameRoom;
  protected String name;
  protected int score = 0;
  protected int rank;

  public Player(String name, int rank, GameRoom gameRoom) {
    this.name = name;
    this.rank = rank;
    this.gameRoom = gameRoom;
    this.gameRoom.add(this);
  }

  public void update() {
    this.rank = gameRoom.getPlayers().indexOf(this) + 1;
  }

  public int compareTo(Player p) {
    return p.score - this.score;
  }

  public String getName() {
    return this.name;
  }

  public int getRank() {
    return rank;
  }

  public int getScore() {
    return score;
  }

  public void addScore(int addition) {
    this.score += addition;
  }
}
