package com.quhaad.scoreboard.service;

import com.quhaad.scoreboard.core.Player;
import java.util.List;

public interface ScoreboardService {
  void roundOver();
  
  List<Player> getPlayers();
}